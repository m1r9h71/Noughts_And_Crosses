package noughtsAndCrosses;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;
public class RepeatAssignment extends Application {
	
	public void start(Stage primaryStage){
		try{
		//BorderPane rp = (BorderPane)FXMLLoader.load(getClass().getResource("nandc.fxml"));
			Parent rp = FXMLLoader.load(getClass().getResource("nandc.fxml"));
		Scene scene = new Scene(rp, 600, 600);
		scene.getStylesheets().add(getClass().getResource("controllercss.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setResizable(false);
		primaryStage.show();
		
	
	}catch(Exception e){}
		
	
	
	
	
	}
	public static void main(String[] args) {
		launch(args);
	}

}
