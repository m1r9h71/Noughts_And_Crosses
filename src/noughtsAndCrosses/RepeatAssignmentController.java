package noughtsAndCrosses;

import java.net.URL;
import java.util.ArrayList;
import java.util.Random;
import java.util.ResourceBundle;

import javax.swing.JOptionPane;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class RepeatAssignmentController extends RepeatAssignment implements Initializable{
	private static final Button Button = null;
	@FXML Button butt1;
	@FXML Button butt2;
	@FXML Button butt3;
	@FXML Button butt4;
	@FXML Button butt5;
	@FXML Button butt6;
	@FXML Button butt7;
	@FXML Button butt8;
	@FXML Button butt9;
	@FXML GridPane threebythree;
	@FXML private ComboBox<String> cb;
	@FXML private boolean play;
	Button clickedOn;
	String label;
	
	
	/*@FXML private void initialize(){
	cb.setValue("Game");
	cb.getItems().addAll(options);
	}*/
	private boolean player1 = true;
	
	String player2;
	//i originally used this method for the combobox but it gave me nothing but errors
	//i changed the code to initializable and left this in because it is called up in the FXML file
	// tried to change code to call up gameclickhandler method for all buttons and combo but it crashed!
	public void gameClickHandler(ActionEvent e){
		//cb.getItems().addAll("Restart", "Two Player", "One Player");
		//cb.getItems().clear();
		//ObservableList<String> options = FXCollections.observableArrayList("Player One","Player Two","Restart");
		//cb.getItems().addAll(options);

		/*String selectedFromCombo = (String) cb.getValue();
		if (selectedFromCombo.equals("Restart")){
			//restartGame(null, e);
			buttonClick(e);
		}else if (selectedFromCombo.equals("Player Two")){
			buttonClick(e);
		}else if (selectedFromCombo.equals("Player One")){
			buttonClick(e);
			//onePlayerGame();
			//ObservableList<Node> buttons = threebythree.getChildren();
			//buttons.forEach(btn -> {
				//((Button)btn).setText("");*/
				//player1 = true;
		
				//buttonClick(e);
			//});
		//}
	} 
	
	
	//use method defined in FXML file for buttons
	public void buttonClick(ActionEvent e){
		//player1=true;
		
		String selectedFromCombo = (String) cb.getValue();
		if(selectedFromCombo != "Player One" && selectedFromCombo != "Player Two" 
				&& selectedFromCombo != "Reset"){
			clickedOn.disableProperty();
		}
		switch (selectedFromCombo){
		
			
		case("Player One"):
		do{	
		clickedOn = (Button)e.getTarget();
		label = clickedOn.getText();
		if(label.equals("")){
		    clickedOn.setText("X");
			clickedOn.setFont(Font.font(50));
			player1 = false;
		    computerPlayer();		
		}
		
		break;
		}while  (lookForAWinner());
		
		
		case("Player Two"):
		clickedOn = (Button)e.getTarget();
		label = clickedOn.getText();
		if ("".equals(label)&& player1){ //if button is empty and player1 turn 
			clickedOn.setText("X");      //enter X when clicked
			clickedOn.setFont(Font.font(50));
			player1 = false;
		}else if("".equals(label)&& !player1){//if button is empty and player2
			clickedOn.setText("O");           //enter O when clicked
			clickedOn.setFont(Font.font(50));
		 player1 = true;
		}
	lookForAWinner();
	
		break;
		//clear all Buttons
		case("Restart"):
		butt1.setText("");
		butt2.setText("");
		butt3.setText("");
		butt4.setText("");
		butt5.setText("");
		butt6.setText("");
		butt7.setText("");
		butt8.setText("");
		butt9.setText("");
		//weHaveAWinner(null, null, null);
		//clickedOn.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
		
		butt1.getStyleClass().remove("winning-combo");
		butt2.getStyleClass().remove("winning-combo");
		butt3.getStyleClass().remove("winning-combo");
		butt4.getStyleClass().remove("winning-combo");
		butt5.getStyleClass().remove("winning-combo");
		butt6.getStyleClass().remove("winning-combo");
		butt7.getStyleClass().remove("winning-combo");
		butt8.getStyleClass().remove("winning-combo");
		butt9.getStyleClass().remove("winning-combo");
		break;
		default:
			System.out.println("Unknown Option Selected, please try again");
		break;
		}
		
}
	//when player 2 has taken their turn the computer chooses a square at random
	private void computerPlayer(){
		//cannot stop this choosing a number already chosen...!
		do{
			
			
				//if(butt1.getText().equals("")&&!"X".equals(butt1)&&!"O".equals(butt1)){
				if(butt1.getText().equals("") && !butt1.getText().equals("X") && !butt1.getText().equals("O")){
					butt1.setText("O");
					butt1.setFont(Font.font(50));
					//compP = true;
					player1 = true;
					
				}
				
			
				//if(butt2.getText().equals("")&&!butt2.getText().equals("X")&&!butt2.getText().equals("O")){
				else if(butt3.getText().equals("") && !butt3.getText().equals("X") && !butt3.getText().equals("O")){
					butt3.setText("O");
					butt3.setFont(Font.font(50));
					//compP = true;
					player1 = true;
					
				}
			
			
				//if(butt3.getText().equals("")&&!butt3.getText().equals("X")&&!butt3.getText().equals("O")){
				else if(butt7.getText().equals("") && !butt7.getText().equals("X") && !butt7.getText().equals("O")){
					butt7.setText("O");
					butt7.setFont(Font.font(50));
					//compP = true;
					player1 = true;
					
				}
				
			
				//if(butt4.getText().equals("")&&!butt4.getText().equals("X")&&!butt4.getText().equals("O")){
				else if(butt9.getText().equals("") && !butt9.getText().equals("X") && !butt9.getText().equals("O")){
					butt9.setText("O");
					butt9.setFont(Font.font(50));
					//compP = true;
					player1 = true;
				
				}
				
		
				//if(butt5.getText().equals("")&&!butt5.getText().equals("X")&&!butt5.getText().equals("O")){
				else if(butt5.getText().equals("") && !butt5.getText().equals("X") && !butt5.getText().equals("O")){
					butt5.setText("O");
					butt5.setFont(Font.font(50));
					//compP = true;
					player1 = true;
					
				}
			
		
				else if(butt2.getText().equals("")&&!butt2.getText().equals("X")&&!butt2.getText().equals("O")){
					butt2.setText("O");
					butt2.setFont(Font.font(50));
					//compP = true;
					player1 = true;
				
				}
			
			
				//if(butt7.getText().equals("")&&!butt7.getText().equals("X")&&!butt7.getText().equals("O")){
				else if(butt4.getText().equals("") && !butt4.getText().equals("X") && !butt4.getText().equals("O")){
					butt4.setText("O");
					butt4.setFont(Font.font(50));
					//compP = true;
					player1 = true;
					
				}
				
		
				//if(butt8.getText().equals("")&&!butt8.getText().equals("X")&&!butt8.getText().equals("O")){
				else if(butt6.getText().equals("") && !butt6.getText().equals("X") && !butt6.getText().equals("O")){
					butt6.setText("O");
					butt6.setFont(Font.font(50));
					//compP = true;
					player1 = true;
					
				}
				
			
				//if(butt9.getText().equals("")&&!butt9.getText().equals("X")&&!butt9.getText().equals("O")){
				else if(butt8.getText().equals("") && !butt8.getText().equals("X") && !butt8.getText().equals("O")){
					butt8.setText("O");
					butt8.setFont(Font.font(50));
					//compP = true;
					player1 = true;
					
	 			}
				//start to block opponents moves
				//top line blocking
				else if (butt1.getText().equals("X") && butt2.getText().equals("X")
						&& butt3.getText().equals("")){
					butt3.setText("O");
					butt3.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt1.getText().equals("X") && butt3.getText().equals("X")
						&& butt2.getText().equals("")){
					butt2.setText("O");
					butt2.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt2.getText().equals("X") && butt3.getText().equals("X")
						&& butt1.getText().equals("")){
					butt1.setText("O");
					butt1.setFont(Font.font(50));
					player1 = true;
				}
				//middle row blocking
				else if (butt4.getText().equals("X") && butt5.getText().equals("X")
						&& butt6.getText().equals("")){
					butt6.setText("O");
					butt6.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt4.getText().equals("X") && butt6.getText().equals("X")
						&& butt5.getText().equals("")){
					butt5.setText("O");
					butt5.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt5.getText().equals("X") && butt6.getText().equals("X")
						&& butt4.getText().equals("")){
					butt4.setText("O");
					butt4.setFont(Font.font(50));
					player1 = true;
				}
				//bottom row blocking
				else if (butt7.getText().equals("X") && butt8.getText().equals("X")
						&& butt9.getText().equals("")){
					butt9.setText("O");
					butt9.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt8.getText().equals("X") && butt9.getText().equals("X")
						&& butt7.getText().equals("")){
					butt7.setText("O");
					butt7.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt7.getText().equals("X") && butt9.getText().equals("X")
						&& butt8.getText().equals("")){
					butt8.setText("O");
					butt8.setFont(Font.font(50));
					player1 = true;
				}
				//diagonal blocking top left to bottom right
				else if (butt1.getText().equals("X") && butt5.getText().equals("X")
						&& butt9.getText().equals("")){
					butt9.setText("O");
					butt9.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt5.getText().equals("X") && butt9.getText().equals("X")
						&& butt1.getText().equals("")){
					butt1.setText("O");
					butt1.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt1.getText().equals("X") && butt9.getText().equals("X")
						&& butt5.getText().equals("")){
					butt5.setText("O");
					butt5.setFont(Font.font(50));
					player1 = true;
				}
				//diagonal blocking top right to bottom left
				else if (butt3.getText().equals("X") && butt5.getText().equals("X")
						&& butt7.getText().equals("")){
					butt7.setText("O");
					butt7.setFont(Font.font(50));
					player1 = true;
				}
				else if (butt5.getText().equals("X") && butt7.getText().equals("X")
						&& butt3.getText().equals("")){
					butt3.setText("O");
					butt3.setFont(Font.font(50));
					player1 = true;
					}
				else if (butt3.getText().equals("X") && butt7.getText().equals("X")
						&& butt5.getText().equals("")){
					butt5.setText("O");
					butt5.setFont(Font.font(50));
					player1 = true;
				}
						
						
				
				
			
		}while (player1 = false);
		
		//while(compP = false);
	}
//checks each row, column and diagonal if there 3 in a row
private boolean lookForAWinner(){
	//Look for 3 of the same in the gridpane threebythree
	//horizontal checks
	//check top row butt1 butt2 butt3
	if (""!=butt1.getText()&& butt1.getText() == butt2.getText()&& butt2.getText()
			== butt3.getText()){
		weHaveAWinner(butt1, butt2, butt3);
		return true;
	}
		//check middle row butt4 butt5 butt6
		if(""!=butt4.getText()&& butt4.getText() == butt5.getText() && butt5.getText()
				== butt6.getText()){
			weHaveAWinner(butt4, butt5, butt6);
			return true;
	}
		//check bottom row butt7 butt8 butt9
		if(""!=butt7.getText()&& butt7.getText() == butt8.getText()&& butt8.getText()
				==butt9.getText()){
			weHaveAWinner(butt7, butt8, butt9);
			return true;
		}
		//vertical checks
		//check left column
		if(""!=butt1.getText()&& butt1.getText() == butt4.getText()&& butt4.getText()
				==butt7.getText()){
			weHaveAWinner(butt1, butt4,butt7);
			return true;
		}
		//check middle column
		if(""!=butt2.getText()&& butt2.getText() == butt5.getText()&&butt5.getText()
				==butt8.getText()){
			weHaveAWinner(butt2,butt5, butt8);
			return true;
		}
		//check right column
		if(""!=butt3.getText()&&butt3.getText() == butt6.getText()&& butt6.getText()
				==butt9.getText()){
			weHaveAWinner(butt3, butt6, butt9);
			return true;
		}
		//diagonal checks
		//top left to bottom right diagonal
		if(""!=butt1.getText()&&butt1.getText() == butt5.getText()&& butt5.getText()
				==butt9.getText()){
			weHaveAWinner(butt1, butt5, butt9);
			return true;
		}
		//top right to bottom left diagonal
		if(""!=butt3.getText()&&butt3.getText() == butt5.getText()&& butt5.getText()
				==butt7.getText()){
			weHaveAWinner(butt3,butt5,butt7);
			
			return true;
		}
		if(""!=butt1.getText()&&""!=butt2.getText()&&""!=butt3.getText()&&
		   ""!=butt4.getText()&&""!=butt6.getText()&&""!=butt7.getText()&&
		   ""!=butt8.getText()&&""!=butt9.getText()){
			staleMate();
			return false;
		}
		return false;
}
	
			

		
	
	


//if a winner is found a new box pops up to tell the players whether O or X has won
private void weHaveAWinner(Button one, Button second, Button third){
	
	{
    //first highlight winning buttons using CSS file
	one.getStyleClass().add("winning-combo");
	second.getStyleClass().add("winning-combo");
	third.getStyleClass().add("winning-combo");	
	//now open new window with winner highlighted
	FlowPane fp =new FlowPane();
	Label lb = new Label();
	lb.setText("The Winner Is: " + one.getText());
	lb.setFont(Font.font(50));
	fp.getChildren().add(lb);
	fp.setAlignment(Pos.CENTER);
	fp.setBackground(new Background(new BackgroundFill(Color.YELLOW, CornerRadii.EMPTY, Insets.EMPTY)));
	Scene s = new Scene(fp, 400,400);
	
	Stage st = new Stage();
	st.setScene(s);
	st.setTitle("We Have A Winner!!!");
	st.show();
	
	}
	
}
	//Scene scene = new Scene(tp,800,500);
	
	
	//if there is no winner a new box pops up to tell the players
private void staleMate(){
	FlowPane fp =new FlowPane();
	Label lb = new Label();
	lb.setText("There is no winner this time!");
	lb.setFont(Font.font(50));
	fp.getChildren().add(lb);
	fp.setAlignment(Pos.CENTER);
	fp.setBackground(new Background(new BackgroundFill(Color.RED, CornerRadii.EMPTY, Insets.EMPTY)));
	Scene s = new Scene(fp, 700,400);
	
	Stage st = new Stage();
	st.setScene(s);
	st.setTitle("Try Again Please");
	st.show();
}


public void initialize(URL arg0, ResourceBundle arg1) {
	// TODO Auto-generated method stub
	//
	ObservableList<String> options = FXCollections.observableArrayList("Player One","Player Two","Restart");
	cb.getItems().addAll(options);
	cb.setValue("Game");
	cb.setOnAction(e -> {
		gameClickHandler(e);
	});
	
	
}

}
